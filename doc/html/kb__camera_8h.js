var kb__camera_8h =
[
    [ "apply_filter", "kb__camera_8h.html#a7b7e3f6bef111932c7e4e45e1e541ce9", null ],
    [ "into_greyscale", "kb__camera_8h.html#afd653987fde99618ada043e5aa6b1d6c", null ],
    [ "kb_camera_init", "kb__camera_8h.html#aae3bd54c1384babc07d263466d5d826d", null ],
    [ "kb_camera_release", "kb__camera_8h.html#a0a9f62e47c659cfda8a617813fca6f22", null ],
    [ "kb_captureStart", "kb__camera_8h.html#a6a4beff6868578e54ef05f1b5290ba55", null ],
    [ "kb_captureStop", "kb__camera_8h.html#ae5d543c2f1e8ba407b09d0b9e1e65aec", null ],
    [ "kb_frameRead", "kb__camera_8h.html#a27c707e097823b674f6290c8741fe3a1", null ],
    [ "save_buffer_to_jpg", "kb__camera_8h.html#a13cf1fd9bd4ab412dda14cd52dfea219", null ],
    [ "take_one_image", "kb__camera_8h.html#a396f3dad7062f3b1d624171bf6652e24", null ]
];
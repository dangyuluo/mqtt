var structknet__bus__s =
[
    [ "bus_info", "structknet__bus__s.html#a972302c9a29d1ec2efabe900fa2b121f", null ],
    [ "close", "structknet__bus__s.html#a1108e5a5ab77542145094942f9774b30", null ],
    [ "devices", "structknet__bus__s.html#a3f677d44ce4880c97f7d989463ed8e64", null ],
    [ "exit", "structknet__bus__s.html#af34fd249a85379eea5b6a52ed0f088d5", null ],
    [ "lock", "structknet__bus__s.html#a93aa23f40ea0f05ac52516130c08c847", null ],
    [ "open", "structknet__bus__s.html#a7fc50fca4a750c881cb0090ade0780a8", null ],
    [ "read", "structknet__bus__s.html#a50326df590322a2f660c34cba8f2bbd8", null ],
    [ "transfer", "structknet__bus__s.html#a5b6329a86987b8d7476feb2a1b90d64d", null ],
    [ "usage_counter", "structknet__bus__s.html#aa175bea2be684190b423d372a1d03967", null ],
    [ "write", "structknet__bus__s.html#ae7ca0ffdda7362253ef6e6c45ab4fdae", null ]
];
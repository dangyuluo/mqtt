var kb__gripper_8c =
[
    [ "kgripper_Arm_Get_Capacity", "kb__gripper_8c.html#ab8059a23067d79f9f24ac325f51b4425", null ],
    [ "kgripper_Arm_Get_Current", "kb__gripper_8c.html#a22afd5309580d5b6c0980708876ebe7c", null ],
    [ "kgripper_Arm_Get_Limits", "kb__gripper_8c.html#a6a70b3691dd9f9df784bbd0ae6063a2e", null ],
    [ "kgripper_Arm_Get_Max_Speed", "kb__gripper_8c.html#a68342e68dd5c1a20fa1b4b6aaf2375aa", null ],
    [ "kgripper_Arm_Get_Order", "kb__gripper_8c.html#a2932ee3898b91b7ed7618f731d8ed834", null ],
    [ "kgripper_Arm_Get_Position", "kb__gripper_8c.html#a9c246a62a0c0c42c2768b773b6325e3d", null ],
    [ "kgripper_Arm_Get_Search_Limit", "kb__gripper_8c.html#a7af459f37040a33b939957f53e53e7c9", null ],
    [ "kgripper_Arm_Get_Speed", "kb__gripper_8c.html#a09034eda465056d138b6a2dad15e6d7a", null ],
    [ "kgripper_Arm_Get_Version", "kb__gripper_8c.html#ab5849a16fa55cdd1a7ef3a8cc9e1cab5", null ],
    [ "kgripper_Arm_Get_Voltage", "kb__gripper_8c.html#a05c5c4d3a3bc266539db451b692357a3", null ],
    [ "kgripper_Arm_OnTarget", "kb__gripper_8c.html#abca79c8b2c59f119be52fdcd70ac458b", null ],
    [ "kgripper_Arm_Set_Max_Speed", "kb__gripper_8c.html#a104425d11259e53207f102fa64a884f3", null ],
    [ "kgripper_Arm_Set_Order", "kb__gripper_8c.html#aac2e21be8b19c86fbbca54211bb9f663", null ],
    [ "kgripper_Arm_Set_Search_Limit", "kb__gripper_8c.html#a25f5e03ad1f244f89c1312e95511d417", null ],
    [ "kgripper_Gripper_Get_Ambiant_IR_Light", "kb__gripper_8c.html#adf725ca410ef6febc2cde9eaeddaee13", null ],
    [ "kgripper_Gripper_Get_Current", "kb__gripper_8c.html#a78586bd8db4dd817a82a823fe68242d2", null ],
    [ "kgripper_Gripper_Get_Distance_Sensors", "kb__gripper_8c.html#ab3342f4ddb5eba186f413be92f1ae202", null ],
    [ "kgripper_Gripper_Get_Limits", "kb__gripper_8c.html#aa4b83cfadcb55722b159745a78d4db28", null ],
    [ "kgripper_Gripper_Get_Order", "kb__gripper_8c.html#a4708ffb6241e065e716c66ea605ba7c4", null ],
    [ "kgripper_Gripper_Get_Position", "kb__gripper_8c.html#a800525e4e793235cac56f546d87ea084", null ],
    [ "kgripper_Gripper_Get_Resistivity", "kb__gripper_8c.html#ad3110ea4d0018756051febfc9c28ebc0", null ],
    [ "kgripper_Gripper_Get_Search_Limit", "kb__gripper_8c.html#aff1e64dc88286b61c42ff362b3413b74", null ],
    [ "kgripper_Gripper_Get_Speed", "kb__gripper_8c.html#ae2fb6852012689a70755b7e5435d7b55", null ],
    [ "kgripper_Gripper_Get_Torque", "kb__gripper_8c.html#aceaea4ed0608e8de918f6fc625009bca", null ],
    [ "kgripper_Gripper_Get_Version", "kb__gripper_8c.html#a9b924649abaa4de346314df66c614b49", null ],
    [ "kgripper_Gripper_Object_Detected", "kb__gripper_8c.html#a1732c2c36dea7fd2f193151af4c9de8e", null ],
    [ "kgripper_Gripper_OnTarget", "kb__gripper_8c.html#a61758a65903f1ac6e19bd4ad758df47c", null ],
    [ "kgripper_Gripper_Set_Order", "kb__gripper_8c.html#ade51ca49bb0f84d446bb6ed8779da82e", null ],
    [ "kgripper_Gripper_Set_Torque", "kb__gripper_8c.html#a76d0160cab1adf655af22b618778ab8e", null ],
    [ "kgripper_GripperSet_Search_Limit", "kb__gripper_8c.html#acb065f76589422c561d01c592e459375", null ],
    [ "kgripper_init", "kb__gripper_8c.html#a7e60d6830cf6158da21865d67cc3490c", null ]
];
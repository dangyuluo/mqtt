var kb__gpio_8c =
[
    [ "gpio_free", "kb__gpio_8c.html#ab6674dc79cdfee0d381910810cbec1a9", null ],
    [ "gpio_request", "kb__gpio_8c.html#adbee45b45d890e04dc3b7f642d2dddba", null ],
    [ "kb_gpio_cleanup", "kb__gpio_8c.html#ae954471145cc6585763aeb01842d61c6", null ],
    [ "kb_gpio_clear", "kb__gpio_8c.html#a4d046c0445b753de64ffb38888ac4bfd", null ],
    [ "kb_gpio_dir", "kb__gpio_8c.html#a1cd4956c014dd1f950f060357edcbe58", null ],
    [ "kb_gpio_dir_val", "kb__gpio_8c.html#af94738e22e2a2a2af6b3a618d94fd4a4", null ],
    [ "kb_gpio_function", "kb__gpio_8c.html#aa35b4abbeed41042307344df3d9fb498", null ],
    [ "kb_gpio_get", "kb__gpio_8c.html#ab27da43aa5cbdd9cc0e9ffdf89e343ae", null ],
    [ "kb_gpio_init", "kb__gpio_8c.html#a985bbe2660af7e41e96720eb8f9393b3", null ],
    [ "kb_gpio_mode", "kb__gpio_8c.html#a0b932e98bd3f105883e02960bcd99b46", null ],
    [ "kb_gpio_set", "kb__gpio_8c.html#a9c15d4671a8c8e9b2cb25245dc878574", null ],
    [ "gFd", "kb__gpio_8c.html#a2ec604fdf54d4bf5ad985df794642920", null ]
];
var crystalfontz634_8c =
[
    [ "c634_backlight", "crystalfontz634_8c.html#a08a028e5e9d148df59281087562de07e", null ],
    [ "c634_backspace", "crystalfontz634_8c.html#a4ec2613f892dbe81563cbab5978cb915", null ],
    [ "c634_clear", "crystalfontz634_8c.html#a2fc246a53e755acb3175ec33f6ac009b", null ],
    [ "c634_close", "crystalfontz634_8c.html#a2bc16303fb4afbd3339bdb684d972f6f", null ],
    [ "c634_contrast", "crystalfontz634_8c.html#a117835efcf153a308c26590d2785d136", null ],
    [ "c634_cr", "crystalfontz634_8c.html#a9e2bb157be61faef28d6a3d43601c72c", null ],
    [ "c634_cursor", "crystalfontz634_8c.html#af259c2b5e72cab5f34a19ebf0e163d1d", null ],
    [ "c634_defineChar", "crystalfontz634_8c.html#a0805875d58b2fca91434871872ccac8c", null ],
    [ "c634_gotoxy", "crystalfontz634_8c.html#a212f80b4cca3f2a737fbcfb2c2497620", null ],
    [ "c634_hide", "crystalfontz634_8c.html#a9dbf16f3f657aaa5e67e69d210b3cf2b", null ],
    [ "c634_home", "crystalfontz634_8c.html#aa7be62f1c4290429c2e477288cc9fe2d", null ],
    [ "c634_lf", "crystalfontz634_8c.html#a772e9fe3c504aff60374cd360c86e4b0", null ],
    [ "c634_open", "crystalfontz634_8c.html#a91f27f71bd7362821dc9b5aa3ec1ae01", null ],
    [ "c634_printf", "crystalfontz634_8c.html#a46480a3d0d285856c3dc54dca1f56af5", null ],
    [ "c634_reboot", "crystalfontz634_8c.html#af80b9c3ba1dcfc5ef6e8c3d508059980", null ],
    [ "c634_restore", "crystalfontz634_8c.html#a0ac39f8649ad8acf0cd14231da0cc79d", null ],
    [ "c634_scrollMode", "crystalfontz634_8c.html#a80148179134324dc9077e8ea05dd4741", null ],
    [ "c634_wrapMode", "crystalfontz634_8c.html#a2c2f3af85beb3f91a5e045146c3409aa", null ]
];
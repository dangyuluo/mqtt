var kb__lrf_8c =
[
    [ "urg_state_t", "structurg__state__t.html", "structurg__state__t" ],
    [ "BAUDRATE", "kb__lrf_8c.html#a734bbab06e1a9fd2e5522db0221ff6e3", null ],
    [ "GPIO_POWER", "kb__lrf_8c.html#a04329bb274c675fbe1263009d560e1f1", null ],
    [ "MAP_MASK", "kb__lrf_8c.html#ae58284531c9467c4f6d7bff45f8d58b4", null ],
    [ "MAP_SIZE", "kb__lrf_8c.html#a0a82fd70402bbdc2259248e735fecbca", null ],
    [ "Timeout", "kb__lrf_8c.html#a99fb83031ce9923c84392b4e92f956b5a98cb58869380e6582c793dcd0e86598c", null ],
    [ "LineLength", "kb__lrf_8c.html#a99fb83031ce9923c84392b4e92f956b5a099017dd2bc8b8e296dcf14399d68326", null ],
    [ "com_connect", "kb__lrf_8c.html#ab9ff2bb9e49eabd6542995d31a14a68d", null ],
    [ "com_recv", "kb__lrf_8c.html#a66ac0065d2b876e666d15fde9335c113", null ],
    [ "com_send", "kb__lrf_8c.html#aa2fcdc24d4bb4a4a10adb307bfc3d1a9", null ],
    [ "urg_sendTag", "kb__lrf_8c.html#ae06abbf73f5746fad36a5510756f1aa0", null ],
    [ "ErrorMessage", "kb__lrf_8c.html#ae5ed924fe4d63b1b774411680dfcc60c", null ],
    [ "HComm", "kb__lrf_8c.html#a343e3ee936b9fb1aa4216637a42848f8", null ],
    [ "kb_lrf_DistanceData", "kb__lrf_8c.html#a24760cfb5cdb7b2edd5689982af47fae", null ],
    [ "kb_lrf_DistanceDataSensor", "kb__lrf_8c.html#ab927664afad638d0c5dc94f232383c7f", null ],
    [ "kb_lrf_DistanceDataSum", "kb__lrf_8c.html#a829b9af6141b02e36b55d020cc8c613d", null ],
    [ "kb_lrf_DistanceGoodCounter", "kb__lrf_8c.html#a0b6f92e179a55fd8bf8f52ed9cdbaebf", null ],
    [ "urg_state", "kb__lrf_8c.html#a7934775a5cae6e9526c4a1484d1b9e10", null ]
];
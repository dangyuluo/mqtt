var kb__socket_8h =
[
    [ "ksock_s", "structksock__s.html", "structksock__s" ],
    [ "MAXPENDING", "kb__socket_8h.html#af77ed3e65a7b1a9341603d152a1404df", null ],
    [ "ksock_t", "kb__socket_8h.html#ac939ccf099731bb7d209097633dcffdb", null ],
    [ "ksock_connect", "kb__socket_8h.html#afa2f4ff49566a3dd3b8dee453649b734", null ],
    [ "ksock_exec_command", "kb__socket_8h.html#a04c7a4782c4ed34429bfa14b7c8bbd46", null ],
    [ "ksock_exec_command_pending", "kb__socket_8h.html#a50b1699aa79204ca924184ed0cae9eb6", null ],
    [ "ksock_next_connection", "kb__socket_8h.html#a2850ce8f7c3e0956de8e706fed1a5922", null ],
    [ "ksock_send_command", "kb__socket_8h.html#a7e3dd402b1d8bf3c03bdf09f1994cf9b", null ],
    [ "ksock_server_open", "kb__socket_8h.html#aeb825bdaacce848699d2334999014012", null ]
];
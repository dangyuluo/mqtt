var structkb__section__config__s =
[
    [ "alias_count", "structkb__section__config__s.html#a7a8e848f8313b88ddc5478e96758fabd", null ],
    [ "aliases", "structkb__section__config__s.html#ac527a523ab7470aea79fc3f2f93adf78", null ],
    [ "device_count", "structkb__section__config__s.html#a6521eaaa117a399953bfa5c1b840ebc7", null ],
    [ "devices", "structkb__section__config__s.html#a349bb458b0d9d453ac29cc101c78e5cf", null ],
    [ "module_bus", "structkb__section__config__s.html#aad5dddf2083fbb27d5a78efa14669f21", null ],
    [ "module_bus_addr", "structkb__section__config__s.html#a4e019a198c35e99e1385bb9a9aff2213", null ],
    [ "next", "structkb__section__config__s.html#a2958bded03f46f957a95b598cf354e37", null ],
    [ "register_count", "structkb__section__config__s.html#a096465d96b4dd820321c4fc7a4dd4e29", null ],
    [ "registers", "structkb__section__config__s.html#a88cea4804ec7e7c7695d3512d763604d", null ]
];
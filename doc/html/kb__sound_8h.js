var kb__sound_8h =
[
    [ "kb_sound_configure", "kb__sound_8h.html#aaed0de4b7a27945af03ed2455d5882c7", null ],
    [ "kb_sound_init", "kb__sound_8h.html#af38eb1cccb9f70c0b6825df37f36d632", null ],
    [ "kb_sound_release", "kb__sound_8h.html#a778b93634e14dbc93e993d175823091f", null ],
    [ "load_wav_file", "kb__sound_8h.html#a14654f579050766b0aebb77a0252ad1f", null ],
    [ "mute_micros", "kb__sound_8h.html#a2f3347fe55fa0a57eb6fed9eaff83ded", null ],
    [ "mute_speaker", "kb__sound_8h.html#aabe185586a9f41e4cc03c93f72e1928c", null ],
    [ "play_buffer", "kb__sound_8h.html#a42d0421906bded38f4c7c63313f7be0e", null ],
    [ "record_buffer", "kb__sound_8h.html#aa877bc68076b9112851efec4865a2bb6", null ],
    [ "save_wav_file", "kb__sound_8h.html#a4bf92ec8a2cfa66f0a112376a011c621", null ],
    [ "set_microphones_volume", "kb__sound_8h.html#aaf556475752973603d20185322db471b", null ],
    [ "set_speakers_volume", "kb__sound_8h.html#a359d75dc5d7d631d00cbd7be8657d621", null ],
    [ "stop_record_play", "kb__sound_8h.html#a8cf8b819e5f13a031832c5de31510f75", null ],
    [ "switch_speakers_ON_OFF", "kb__sound_8h.html#a64b3aeda89ec0b7cff57de10b23deb57", null ],
    [ "wait_end_of_play", "kb__sound_8h.html#a4e7f4c407e038f9b8b354715c02d0219", null ]
];
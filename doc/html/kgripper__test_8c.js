var kgripper__test_8c =
[
    [ "demo", "kgripper__test_8c.html#a2e475ec8766b463001bbb36cc8fd0db3", null ],
    [ "get", "kgripper__test_8c.html#a8ea1f212915bd4e29a8b1add479918ec", null ],
    [ "grip", "kgripper__test_8c.html#a3313e2f67b3a4170175386b8d009954d", null ],
    [ "help", "kgripper__test_8c.html#ae3d6a45deeef910eb59d5be977b0dfa1", null ],
    [ "initGripper", "kgripper__test_8c.html#a85037591f56d137860d6e37ef46c8eff", null ],
    [ "main", "kgripper__test_8c.html#accdee5855b5aa8b3fa2be41ccf9be430", null ],
    [ "movearm", "kgripper__test_8c.html#acf509e5ed94295523366f90770d8011a", null ],
    [ "movegrip", "kgripper__test_8c.html#abfadf569f2fcf9c6569404831cdd2e61", null ],
    [ "quit", "kgripper__test_8c.html#ae92fb5ec62807efb0ce1b2171784066f", null ],
    [ "revisionOS", "kgripper__test_8c.html#aa1602bd380ba8d38f892f2e4c65cff46", null ],
    [ "searchLimit", "kgripper__test_8c.html#af0171a52b002e9e66750aad6305c8896", null ],
    [ "sensor", "kgripper__test_8c.html#a44a7b4a4ffd387895f43c4d53ce1afde", null ],
    [ "setMaxSpeed", "kgripper__test_8c.html#afdf6c027e4507cfcf38f90ee86f2577d", null ],
    [ "setTorque", "kgripper__test_8c.html#af11e46c4569dac14b77d63ceed5f88d6", null ],
    [ "status", "kgripper__test_8c.html#abc9e348a7a7fb4c413679ef7ee4a4ead", null ],
    [ "Arm", "kgripper__test_8c.html#af1b7c1ad4cd3e30dd38234ac567d78b0", null ],
    [ "cmds", "kgripper__test_8c.html#a5532197815d495c65c2c74f6cd107825", null ],
    [ "Gripper", "kgripper__test_8c.html#af10d45ab702d05ec0e17904699adbc59", null ],
    [ "quitReq", "kgripper__test_8c.html#a7afd688646baddcb54287c2eec6949a9", null ]
];
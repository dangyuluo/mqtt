var i2ccom_8c =
[
    [ "i2c_close", "i2ccom_8c.html#aa00f0ec5de79f6c8535337e1dbe5b34f", null ],
    [ "i2c_exists", "i2ccom_8c.html#a1fef7a079c461a96208a89e2ee716037", null ],
    [ "i2c_llread", "i2ccom_8c.html#a23ca67af5334e15e71b4a3712031f0dd", null ],
    [ "i2c_lltransfer", "i2ccom_8c.html#a09a5dcd5737f6ffdb04a50c3f4e3c797", null ],
    [ "i2c_llwrite", "i2ccom_8c.html#a150258c37e58c033fc0af8b70e88821f", null ],
    [ "i2c_open", "i2ccom_8c.html#ae3dac8f7f424c2935bd312a7c8ddcde6", null ],
    [ "i2c_priv_select_device", "i2ccom_8c.html#a3b656b62a135d979f75400c38931187d", null ],
    [ "i2c_scan", "i2ccom_8c.html#a4c8118b9e0cbe96062442c7d64fd2532", null ]
];
------------------------------------------------------------------------------
$Id: README.kteam,v 2.0 2012/07/05 16:57:25 jt Exp $
------------------------------------------------------------------------------
$Date: 2012/07/05 16:57:25 $ $Revision: 2.0$ $Author: jt $
------------------------------------------------------------------------------

*** khepera Library Compilation and Installation quick guide ***

------------------------------------------------------------------------------
A] Library compilation
------------------------------------------------------------------------------

To compile the khepera library, follow these steps:

-------------------------------- 
1 - Install the required compiler
--------------------------------

To compile the khepera library for the khepera platform, a cross compiler
arm-linux-gcc is required. The library can be compiled to run on other
platforms using another compiler, in that case the main Makefile should be
edited.  The arm-linux-gcc cross compiler is available from K-Team website. It
can be installed as a pre-compiled binary or compiled from scratch using the
source distribution. Both installation methods are described in the khepera
user manual at chapter "ARM Linux Toolchain".

-------------------------------- 
2 - Edit the Makefile
--------------------------------

The default settings in the Makefile should be correct to compile the
libkhepera using arm-linux-gcc. Some settings can be changed according to your
system:

- TARGET_SYSTEM: cross compile for the given target 
- KB_CONFIG_DIRECTORY: location for the libkhepera config files 
- KHEPERA_TOOLS: location for the khepera-tools package	


-------------------------------- 
3 - Build the library
--------------------------------

Once properly configured, the library can be completely compiled as well as all
the example programs by using:

make

To start over from a clean distribution use:

make clean

The compilation results are available in:

 ./libkhepera/build-($TARGET_SYSTEM) 

That directory will contain:

- All libkhepera related headers in include/khepera, they should be copied to
the khepera filesystem to compile program with a native compiler (see section B).

- The library itself in lib/


-------------------------------- 
4 - Build the documentation
--------------------------------

Before building the documentation, the doxygen package should be properly
installed. The doxygen package can be downloaded from the doxygen website
www.doxygen.org. The dot command is also required to build the documentation,
the dot command is part of the graphviz package, which is available for most
common distribution. The Doxygen documentation can be created from the sources
using:

make doc

The resulting documentation will be available in ./libkhepera/doc


------------------------------------------------------------------------------
B] Library installation
------------------------------------------------------------------------------

The library must be installed on the khepera filesystem before being used by
other programs, the libkhepera Makefile will both provide static and shared
library. Using the shared library mechanism is highly recommended to save
memory space on the khepera. To install the library to the khepera system,
follow the following steps.

-------------------------------- 
1 - Copy the library
--------------------------------

The content of ./libkhepera/build-($TARGET_SYSTEM)/lib should be copied to the
khepera /usr/lib directory.

-------------------------------- 
2 - Copy the configuration files
--------------------------------

The content of ./libkhepera/config should be copied to the khepera
($KB_CONFIG_DIRECTORY). That directory is /etc/libkhepera by default.

-------------------------------- 
3 - Copy the headers
--------------------------------

To compile khepera programs on the khepera platform, using a native compiler,
the libkhepera header files should be copied to the khepera system. This step
is not necessary if you will only cross-compile your programs.  The khepera
directory ./libkhepera/build-($TARGET_SYSTEM)/include should be copied to the
khepera /usr/include directory.

------------------------------------------------------------------------------
C] Program compilation
------------------------------------------------------------------------------

The easiest way to compile libkhepera programs is to use the program template
that is provided with the libkhepera distribution.  In the
./libkhepera/template directory, edit the prog-template.c source and use:

make template 

This will build the template program using libkhepera shared library. To build
the template program using static linking to the libkhepera, use:

make template-static
